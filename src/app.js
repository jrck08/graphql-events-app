// const express = require("express");
import express from "express";
import graphqlHttp from "express-graphql";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import { schema, rootValue } from "./schema";

const app = express();
const port = 3000;

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@graphql-event-test-app-rau77.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`,
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(res => {
    // app.listen(3000);
    console.log(res);
  })
  .catch(err => console.log(err));

app.use(bodyParser.json());
app.get("/", function(req, res) {
  res.json({ message: "Hello Jarvis!" });
});

app.use(
  "/graphql",
  graphqlHttp({
    schema,
    rootValue,
    graphiql: true
  })
);

app.listen(port, function() {
  console.log("App is running at port " + port);
});
