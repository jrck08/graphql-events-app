import { buildSchema } from "graphql";
import Event from "./models/event";

let events = [];

const schema = buildSchema(`
  type Event {
      _id: ID!
      title: String!
      description: String!
      price: Float!
      date: String!
    }

    input EventInput{
      title: String!
      description: String!
      price: Float!
      date: String
    }

    type rootQuery {
      events: [Event!]!
      event(title: String): Event!
    }

    type rootMutation {
      createEvent(eventInput: EventInput): Event
    }

    fragment EvenFields on Event {
      title
      description
      price
    }

    schema {
      query: rootQuery
      mutation: rootMutation
    }
`);

// resolvers

const rootValue = {
  events: () => {
    return Event.find()
      .then(events => {
        return events.map(event => {
          return { ...event._doc, _id: event.id };
        });
      })
      .catch(err => console.log(err));
  },
  event: args => {
    return Event.findOne({ title: args.title })
      .then(event => {
        console.log(event);
        return { ...event._doc };
      })
      .catch(err => console.log(err));
  },
  createEvent: args => {
    const event = new Event({
      title: args.eventInput.title,
      description: args.eventInput.description,
      price: +args.eventInput.price,
      date: new Date(args.eventInput.date)
    });

    return event
      .save()
      .then(res => {
        console.log(res);
        return { ...res._doc };
      })
      .catch(err => {
        console.log(err);
        throw err;
      });
  }
};

export { schema, rootValue };
