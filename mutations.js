// Sample mutation in graphiql
`
mutation {
  createEvent(eventInput:{
    title: "Some sh*t"
    description: "really some sh*t"
    price: 99.99
    
  }) {
    title
    description
    date
  }
}`;

`mutation {
  createEvent(eventInput: {title: "Learn GraphQL with Express", description: "This tutorial will help you learn GraphQL with Express JS.", price: 99.99, date: "2020-02-26T06:03:12.787Z"}) {
    title
    description
    date
    price
  }
}
`;
